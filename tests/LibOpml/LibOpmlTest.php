<?php

namespace marienfressinaud\LibOpml;

class LibOpmlTest extends \PHPUnit\Framework\TestCase
{
    public function setUp(): void
    {
        ini_set('date.timezone', 'UTC');
    }

    public function testParseFileReturnsAnArray()
    {
        $filepath = __DIR__ . '/../../examples/example.opml.xml';

        $libopml = new LibOpml();
        $opml_array = $libopml->parseFile($filepath);

        $this->assertSame('2.0', $opml_array['version']);

        $this->assertSame('My OPML', $opml_array['head']['title']);
        $this->assertSame(1658758866, $opml_array['head']['dateCreated']->getTimestamp());

        $outlines = $opml_array['body'];
        $this->assertSame(1, count($outlines));
        $this->assertSame('Newspapers', $outlines[0]['text']);

        $suboutlines = $outlines[0]['@outlines'];
        $this->assertSame(4, count($suboutlines));
        $this->assertSame('El País', $suboutlines[0]['text']);
        $this->assertSame('rss', $suboutlines[0]['type']);
        $this->assertSame('https://feeds.elpais.com/mrss-s/pages/ep/site/elpais.com/', $suboutlines[0]['xmlUrl']);
        $this->assertSame('https://elpais.com/', $suboutlines[0]['htmlUrl']);
    }

    public function testParseFileFailsIfFileDoesNotExist()
    {
        $filepath = 'not/a/file.opml';

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("{$filepath} cannot be found");

        $libopml = new LibOpml();
        $libopml->parseFile($filepath);
    }

    public function testParseStringReturnsAnArray()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <title>My OPML</title>
        <dateCreated>Sat, 29 Mar 2014 15:42:42 +0000</dateCreated>
        <dateModified>Sat, 29 Mar 2014 15:42:42 +0000</dateModified>
    </head>
    <body>
        <outline text="Newspapers">
            <outline text="El País" type="rss" xmlUrl="https://feeds.elpais.com/mrss-s/pages/ep/site/elpais.com/" />
            <outline text="Le Monde" type="rss" xmlUrl="https://www.lemonde.fr/rss/une.xml" />
        </outline>
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('2.0', $opml_array['version']);

        $this->assertSame('My OPML', $opml_array['head']['title']);
        $this->assertSame(1396107762, $opml_array['head']['dateCreated']->getTimestamp());
        $this->assertSame(1396107762, $opml_array['head']['dateModified']->getTimestamp());

        $outlines = $opml_array['body'];
        $this->assertSame(1, count($outlines));
        $this->assertSame('Newspapers', $outlines[0]['text']);

        $suboutlines = $outlines[0]['@outlines'];
        $this->assertSame(2, count($suboutlines));
        $this->assertSame('El País', $suboutlines[0]['text']);
        $this->assertSame('https://feeds.elpais.com/mrss-s/pages/ep/site/elpais.com/', $suboutlines[0]['xmlUrl']);
        $this->assertSame('Le Monde', $suboutlines[1]['text']);
        $this->assertSame('https://www.lemonde.fr/rss/une.xml', $suboutlines[1]['xmlUrl']);
    }

    public function testParseStringHandles11As10()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="1.1">
    <head />
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('1.0', $opml_array['version']);
    }

    public function testParseStringAcceptsElementsInNamespace()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0" xmlns:test="https://example.com/test">
    <head>
        <title>My OPML</title>
        <test:foo>bar</test:foo>
    </head>
    <body>
        <outline text="Newspapers" test:spam="egg"/>
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('2.0', $opml_array['version']);
        $this->assertSame([
            'test' => 'https://example.com/test',
        ], $opml_array['namespaces']);

        $this->assertSame('My OPML', $opml_array['head']['title']);
        $this->assertSame('bar', $opml_array['head']['test:foo']);

        $outlines = $opml_array['body'];
        $this->assertSame(1, count($outlines));
        $this->assertSame('Newspapers', $outlines[0]['text']);
        $this->assertSame('egg', $outlines[0]['test:spam']);
    }

    public function testParseStringHandlesExpansionState()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <expansionState>1,42</expansionState>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $head = $opml_array['head'];
        $this->assertSame([1, 42], $head['expansionState']);
    }

    /**
     * @dataProvider numericHeadElementsProvider
     */
    public function testParseStringHandlesHeadNumbersElements($key)
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <{$key}>42</{$key}>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $head = $opml_array['head'];
        $this->assertSame(42, $head[$key]);
    }

    public function testParseStringHandlesOutlineCreatedAttribute()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" created="Sat, 29 Mar 2014 15:42:42 +0000" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame('Newspapers', $outlines[0]['text']);
        $this->assertSame(1396107762, $outlines[0]['created']->getTimestamp());
    }

    public function testParseStringHandlesOutlineCategoryAttribute()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" category="foo, bar" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame('Newspapers', $outlines[0]['text']);
        $this->assertSame(['foo', 'bar'], $outlines[0]['category']);
    }

    public function testParseStringHandlesOutlineIsCommentAttribute()
    {
        $values = ['true', 'false'];
        shuffle($values);
        $value = $values[0];
        $bool_value = $value === 'true';
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" isComment="{$value}" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame('Newspapers', $outlines[0]['text']);
        $this->assertSame($bool_value, $outlines[0]['isComment']);
    }

    public function testParseStringHandlesOutlineIsBreakpointAttribute()
    {
        $values = ['true', 'false'];
        shuffle($values);
        $value = $values[0];
        $bool_value = $value === 'true';
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" isBreakpoint="{$value}" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame('Newspapers', $outlines[0]['text']);
        $this->assertSame($bool_value, $outlines[0]['isBreakpoint']);
    }

    public function testParseStringForcesTypeToLowerCase()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="1.1">
    <head />
    <body>
        <outline text="Newspapers" type="FOO" />
    </body>
</opml>
XML;

        $libopml = new LibOpml();
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame('Newspapers', $outlines[0]['text']);
        $this->assertSame('foo', $outlines[0]['type']);
    }

    public function testParseStringWorksWhenStrictFalseIfVersionIsMissing()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml>
    <head />
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('', $opml_array['version']);
    }

    public function testParseStringWorksWhenStrictFalseIfVersionIsInvalid()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2">
    <head />
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('2', $opml_array['version']);
    }

    public function testParseStringWorksWhenStrictFalseIfHeadIsMissing()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([], $opml_array['head']);
    }

    public function testParseStringWorksWhenStrictFalseIfHeadContainsInvalidElement()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <foo>bar</foo>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('bar', $opml_array['head']['foo']);
    }

    public function testParseStringWorksWhenStrictFalseIfHeadDateElementCannotBeParsed()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <dateCreated>not a date</dateCreated>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('not a date', $opml_array['head']['dateCreated']);
    }

    public function testParseStringWorksWhenStrictFalseIfHeadOwnerEmailElementIsNotAnEmail()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <ownerEmail>not an email</ownerEmail>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('not an email', $opml_array['head']['ownerEmail']);
    }

    public function testParseStringWorksWhenStrictFalseIfHeadOwnerIdElementIsNotAHttpAddress()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <ownerId>not an address</ownerId>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('not an address', $opml_array['head']['ownerId']);
    }

    public function testParseStringWorksWhenStrictFalseIfHeadDocsElementIsNotAHttpAddress()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <docs>not an address</docs>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('not an address', $opml_array['head']['docs']);
    }

    public function testParseStringWorksWhenStrictFalseIfExpansionStateIsNotANumericList()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <expansionState>1,foo</expansionState>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([1, 'foo'], $opml_array['head']['expansionState']);
    }

    /**
     * @dataProvider numericHeadElementsProvider
     */
    public function testParseStringWorksWhenStrictFalseIfNumberElementsAreNotNumbers($key)
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <{$key}>foo</{$key}>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame('foo', $opml_array['head'][$key]);
    }

    public function testParseStringWorksWhenStrictFalseIfBodyIsMissing()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfBodyIsEmpty()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfBodyContainsNonOutlineElements()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
        <outline text="Newspapers" />
        <foo text="Music" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineCreatedAttributeIsNotADate()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" created="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'created' => 'foo'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineIsCommentAttributeIsNotBoolean()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" isComment="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'isComment' => 'foo'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineIsBreakpointAttributeIsNotBoolean()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" isBreakpoint="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'isBreakpoint' => 'foo'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineContainsNonOutlineElements()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
        <outline text="Newspapers">
            <foo text="Music" />
        </outline>
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineTypeIsRssAndXmlUrlIsMissing()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="rss" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'type' => 'rss'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineTypeIsRssAndXmlUrlIsInvalid()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="rss" xmlUrl="not an address" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'type' => 'rss', 'xmlUrl' => 'not an address'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineTypeIsLinkAndUrlIsMissing()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="link" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'type' => 'link'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineTypeIsLinkAndUrlIsInvalid()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="link" url="not an address" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'type' => 'link', 'url' => 'not an address'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineTypeIsIncludeAndUrlIsMissing()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="include" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'type' => 'include'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineTypeIsIncludeAndUrlIsInvalid()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="include" url="not an address" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $this->assertSame([
            ['text' => 'Newspapers', 'type' => 'include', 'url' => 'not an address'],
        ], $opml_array['body']);
    }

    public function testParseStringWorksWhenStrictFalseIfOutlineDoesNotHaveTextAttribute()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head/>
    <body>
        <outline foo="bar"/>
    </body>
</opml>
XML;

        $libopml = new LibOpml(false);
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame(1, count($outlines));
        $this->assertSame('bar', $outlines[0]['foo']);
    }

    public function testParseStringWorksWhenStrictTrueAndVersion10IfOutlineDoesNotHaveTextAttribute()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="1.0">
    <head />
    <body>
        <outline title="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $opml_array = $libopml->parseString($xml);

        $outlines = $opml_array['body'];
        $this->assertSame(1, count($outlines));
        $this->assertSame('Newspapers', $outlines[0]['title']);
    }

    public function testParseStringFailsIfEmptyString()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML string is not valid XML');

        $xml = '';

        $libopml = new LibOpml();
        $libopml->parseString($xml);
    }

    public function testParseStringFailsIfInvalidXml()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML string is not valid XML');

        $xml = 'not xml';

        $libopml = new LibOpml();
        $libopml->parseString($xml);
    }

    public function testParseStringFailsIfElementNamespaceIsNotDeclared()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML string is not valid XML');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <title>My OPML</title>
        <test:foo>bar</test:foo>
    </head>
    <body>
        <outline text="Newspapers"/>
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsIfAttributeNamespaceIsNotDeclared()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML string is not valid XML');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <title>My OPML</title>
    </head>
    <body>
        <outline text="Newspapers" test:foo="bar"/>
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfVersionIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML version attribute is required');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml>
    <head />
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfVersionIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML supported versions are 1.0 and 2.0');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2">
    <head />
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfHeadIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML must contain one and only one head element');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <body>
        <outline text="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfHeadContainsInvalidElement()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head foo element is not part of the specification');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <foo>bar</foo>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfHeadDateElementCannotBeParsed()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head dateCreated element must be a valid RFC822 or RFC1123 date');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <dateCreated>not a date</dateCreated>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOwnerEmailElementIsNotAnEmail()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head ownerEmail element must be an email address');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <ownerEmail>not an address</ownerEmail>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOwnerIdElementIsNotAHttpAddress()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head ownerId element must be a HTTP address');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <ownerId>not an address</ownerId>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfDocsElementIsNotAHttpAddress()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head docs element must be a HTTP address');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <docs>not an address</docs>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfExpansionStateIsNotANumericList()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head expansionState element must be a list of numbers');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <expansionState>1,foo</expansionState>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    /**
     * @dataProvider numericHeadElementsProvider
     */
    public function testParseStringFailsWhenStrictTrueIfNumberElementsAreNotNumbers($key)
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("OPML head {$key} element must be a number");

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <{$key}>foo</{$key}>
    </head>
    <body>
        <outline text="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfBodyIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML must contain one and only one body element');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfBodyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML body element must contain at least one outline element');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfBodyContainsNonOutlineElements()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML body element can only contain outline elements');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
        <outline text="Newspapers" />
        <foo text="Music" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineDoesNotHaveTextAttribute()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline text attribute is required');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
        <outline title="Newspapers" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTextAttributeIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline text attribute is required');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
        <outline text="" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineCreatedAttributeIsNotADate()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline created attribute must be a valid RFC822 or RFC1123 date');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" created="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineIsCommentAttributeIsNotBoolean()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline isComment attribute must be a boolean (true or false)');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" isComment="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailWhenStrictTrueIfOutlineIsBreakpointAttributeIsNotBoolean()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline isBreakpoint attribute must be a boolean (true or false)');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" isBreakpoint="foo" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineContainsNonOutlineElements()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML body element can only contain outline elements');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head />
    <body>
        <outline text="Newspapers">
            <foo text="Music" />
        </outline>
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTypeIsRssAndXmlUrlIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline xmlUrl attribute is required when type is "rss"');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="rss" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTypeIsRssAndXmlUrlIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline xmlUrl attribute must be a HTTP address when type is "rss"');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="rss" xmlUrl="not an address" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTypeIsLinkAndUrlIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute is required when type is "link"');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="link" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTypeIsLinkAndUrlIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute must be a HTTP address when type is "link"');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="link" url="not an address" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTypeIsIncludeAndUrlIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute is required when type is "include"');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="include" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testParseStringFailsWhenStrictTrueIfOutlineTypeIsIncludeAndUrlIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute must be a HTTP address when type is "include"');

        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
    </head>
    <body>
        <outline text="Newspapers" type="include" url="not an address" />
    </body>
</opml>
XML;

        $libopml = new LibOpml(true);
        $libopml->parseString($xml);
    }

    public function testRenderReturnsExpectedString()
    {
        $date = new \DateTime('2022-05-22 18:00:00');
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'dateCreated' => $date,
                'dateModified' => $date,
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                    '@outlines' => [
                        ['text' => 'Le Monde'],
                    ],
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <dateCreated>Sun, 22 May 2022 18:00:00 +0000</dateCreated>
    <dateModified>Sun, 22 May 2022 18:00:00 +0000</dateModified>
  </head>
  <body>
    <outline text="Newspapers">
      <outline text="Le Monde"/>
    </outline>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderCanReturnADomDocument()
    {
        $date = new \DateTime('2022-05-22 18:00:00');
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'dateCreated' => $date,
                'dateModified' => $date,
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                    '@outlines' => [
                        ['text' => 'Le Monde'],
                    ],
                ],
            ],
        ];

        $libopml = new LibOpml();
        $dom_document = $libopml->render($opml, true);

        $this->assertSame('DOMDocument', get_class($dom_document));
        $opml = $dom_document->documentElement;
        $head = $opml->getElementsByTagName('head')[0];
        $head_elements = $head->childNodes;
        $this->assertSame('My OPML', $head_elements[0]->nodeValue);
        $this->assertSame('Sun, 22 May 2022 18:00:00 +0000', $head_elements[1]->nodeValue);
        $this->assertSame('Sun, 22 May 2022 18:00:00 +0000', $head_elements[2]->nodeValue);
        $body = $opml->getElementsByTagName('body')[0];
        $body_elements = $body->childNodes;
        $this->assertSame('Newspapers', $body_elements[0]->getAttribute('text'));
        $this->assertSame(1, count($body_elements[0]->childNodes));
    }

    public function testRenderAcceptsNamespaces()
    {
        $opml = [
            'namespaces' => [
                'test' => 'https://example.com/test',
            ],
            'head' => [
                'title' => 'My OPML',
                'test:foo' => 'bar',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                    'test:spam' => 'egg',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml xmlns:test="https://example.com/test" version="2.0">
  <head>
    <title>My OPML</title>
    <test:foo>bar</test:foo>
  </head>
  <body>
    <outline text="Newspapers" test:spam="egg"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(true);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderAcceptsVersion()
    {
        $opml = [
            'version' => '1.0',
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="1.0">
  <head>
    <title>My OPML</title>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderTransformsVersion11In10()
    {
        $opml = [
            'version' => '1.1',
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="1.0">
  <head>
    <title>My OPML</title>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderRendersExpansionStateCorrectly()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'expansionState' => [1, 42],
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <expansionState>1, 42</expansionState>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    /**
     * @dataProvider numericHeadElementsProvider
     */
    public function testRenderRendersNumericHeadElementsCorrectly($key)
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                $key => 42,
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <{$key}>42</{$key}>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderRendersOutlineAttributeCreatedCorrectly()
    {
        $date = new \DateTime('2022-05-22 18:00:00');
        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'created' => $date,
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="Newspapers" created="Sun, 22 May 2022 18:00:00 +0000"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderRendersOutlineAttributeArraysCorrectly()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'category' => ['foo', 'bar'],
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="Newspapers" category="foo, bar"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderRendersOutlineAttributeIsCommentCorrectly()
    {
        $values = [true, false];
        shuffle($values);
        $value = $values[0];
        $str_value = $value ? 'true' : 'false';
        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'isComment' => $value,
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="Newspapers" isComment="{$str_value}"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderRendersOutlineAttributeIsBreakpointCorrectly()
    {
        $values = [true, false];
        shuffle($values);
        $value = $values[0];
        $str_value = $value ? 'true' : 'false';
        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'isBreakpoint' => $value,
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="Newspapers" isBreakpoint="{$str_value}"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml();
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndVersionIsInvalid()
    {
        $opml = [
            'version' => 'foo',
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="foo">
  <head>
    <title>My OPML</title>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadContainsUnknownElement()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'foo' => 'bar',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <foo>bar</foo>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadDateElementsAreNotDateTime()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'dateCreated' => 'foo',
                'dateModified' => 'bar',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <dateCreated>foo</dateCreated>
    <dateModified>bar</dateModified>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadOwnerEmailElementIsNotAnEmail()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'ownerEmail' => 'not an email',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <ownerEmail>not an email</ownerEmail>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadOwnerIdElementIsNotAHttpAddress()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'ownerId' => 'not an address',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <ownerId>not an address</ownerId>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadDocsElementIsNotAHttpAddress()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'docs' => 'not an address',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <docs>not an address</docs>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadExpansionStateIsNotAnArray()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'expansionState' => 'foo',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <expansionState>foo</expansionState>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndHeadExpansionStateContainsNotNumbers()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                'expansionState' => ['foo', 42],
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <expansionState>foo, 42</expansionState>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    /**
     * @dataProvider numericHeadElementsProvider
     */
    public function testRenderWorksWhenStrictFalseAndHeadNumberElementsAreNotNumbers($key)
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
                $key => 'foo'
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
    <$key>foo</$key>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndBodyIsMissing()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
  </head>
  <body/>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndBodyIsEmpty()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
  </head>
  <body/>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTextIsMissing()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                ['foo' => 'bar'],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
  </head>
  <body>
    <outline foo="bar"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictTrueAndVersionIs10IfTextAttributeIsMissing()
    {
        $opml = [
            'version' => '1.0',
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                ['foo' => 'bar'],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="1.0">
  <head>
    <title>My OPML</title>
  </head>
  <body>
    <outline foo="bar"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(true);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndCreatedIsNotDateTime()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'created' => '2022-06-02',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" created="2022-06-02"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndIsCommentOrIsBreakpointIsNotBoolean()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'isComment' => 'bar',
                    'isBreakpoint' => 'baz',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" isComment="bar" isBreakpoint="baz"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTypeIsRssAndXmlUrlIsMissing()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'type' => 'rss',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" type="rss"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTypeIsRssAndXmlUrlIsInvalid()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'type' => 'rss',
                    'xmlUrl' => 'not an address',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" type="rss" xmlUrl="not an address"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTypeIsLinkAndUrlIsMissing()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'type' => 'link',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" type="link"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTypeIsLinkAndUrlIsInvalid()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'type' => 'link',
                    'url' => 'not an address',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" type="link" url="not an address"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTypeIsIncludeAndUrlIsMissing()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'type' => 'include',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" type="include"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderWorksWhenStrictFalseAndTypeIsIncludeAndUrlIsInvalid()
    {
        $opml = [
            'body' => [
                [
                    'text' => 'foo',
                    'type' => 'include',
                    'url' => 'not an address',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head/>
  <body>
    <outline text="foo" type="include" url="not an address"/>
  </body>
</opml>

XML;

        $libopml = new LibOpml(false);
        $string = $libopml->render($opml);

        $this->assertSame($string, $expected);
    }

    public function testRenderFailsIfElementNamespaceIsNotDeclared()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML namespace test is not declared');

        $opml = [
            'namespaces' => [
            ],
            'head' => [
                'title' => 'My OPML',
                'test:foo' => 'bar',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml();
        $libopml->render($opml);
    }

    public function testRenderFailsIfAttributeNamespaceIsNotDeclared()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML namespace test is not declared');

        $opml = [
            'namespaces' => [
            ],
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                    'test:foo' => 'bar',
                ],
            ],
        ];

        $libopml = new LibOpml();
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfVersionIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML supported versions are 1.0 and 2.0');

        $opml = [
            'version' => 'foo',
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfUnknownHeadElements()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head foo element is not part of the specification');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'foo' => 'bar',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfDateElementsAreNotDateTime()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head dateCreated element must be a DateTime');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'dateCreated' => 'Sun, 22 May 2022 18:00:00 +0000',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfOwnerEmailIsNotAnEmail()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head ownerEmail element must be an email address');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'ownerEmail' => 'not an email',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfOwnerIdIsNotAHttpAddress()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head ownerId element must be a HTTP address');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'ownerId' => 'not an address',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfDocsIsNotAHttpAddress()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head docs element must be a HTTP address');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'docs' => 'not an address',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfExpansionStateIsNotAnArray()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head expansionState element must be an array of integers');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'expansionState' => 42,
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfExpansionStateContainsNotNumbers()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML head expansionState element must be an array of integers');

        $opml = [
            'head' => [
                'title' => 'My OPML',
                'expansionState' => [42, 'foo'],
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    /**
     * @dataProvider numericHeadElementsProvider
     */
    public function testRenderFailsWhenStrictTrueIfNumericHeadElementsAreNotNumbers($key)
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("OPML head {$key} element must be an integer");

        $opml = [
            'head' => [
                'title' => 'My OPML',
                $key => 'foo',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfBodyIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML array must contain a body key');

        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfBodyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML body element must contain at least one outline array');

        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsIfOutlineIsNotAnArray()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline element must be defined as an array');

        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                'foo'
            ],
        ];

        $libopml = new LibOpml();
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfOutlineDoesNotHaveTextAttribute()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline text attribute is required');

        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                ['foo' => 'bar'],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfOutlineTextAttributeIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline text attribute is required');

        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                ['text' => ''],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfCreatedAttributesIsNotDateTime()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline created attribute must be a DateTime');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'created' => 'Sun, 22 May 2022 18:00:00 +0000',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfIsCommentAttributeIsNotBoolean()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline isComment attribute must be a boolean');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'isComment' => 'foo',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfIsBreakpointAttributeIsNotBoolean()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline isBreakpoint attribute must be a boolean');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'isBreakpoint' => 'foo',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfTypeAttributesIsRssAndXmlUrlIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline xmlUrl attribute is required when type is "rss"');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'type' => 'rss',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfTypeAttributesIsRssAndXmlUrlIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline xmlUrl attribute must be a HTTP address when type is "rss"');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'type' => 'rss',
                    'xmlUrl' => 'not an address',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfTypeAttributesIsLinkAndUrlIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute is required when type is "link"');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'type' => 'link',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfTypeAttributesIsLinkAndUrlIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute must be a HTTP address when type is "link"');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'type' => 'link',
                    'url' => 'not an address',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfTypeAttributesIsIncludeAndUrlIsMissing()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute is required when type is "include"');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'type' => 'include',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsWhenStrictTrueIfTypeAttributesIsIncludeAndUrlIsInvalid()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline url attribute must be a HTTP address when type is "include"');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    'type' => 'include',
                    'url' => 'not an address',
                ],
            ],
        ];

        $libopml = new LibOpml(true);
        $libopml->render($opml);
    }

    public function testRenderFailsIfSubOutlinesAreNotArrays()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('OPML outline element must be defined as an array');

        $opml = [
            'body' => [
                [
                    'text' => 'Newspapers',
                    '@outlines' => 'foo',
                ],
            ],
        ];

        $libopml = new LibOpml();
        $libopml->render($opml);
    }

    /**
     * @dataProvider validUrlsProvider
     */
    public function testCheckHttpAddress($url)
    {
        $libopml = new LibOpml();
        $result = $libopml->checkHttpAddress($url);

        $this->assertTrue($result);
    }

    /**
     * @dataProvider invalidUrlsProvider
     */
    public function testCheckHttpAddressFailsWithInvalidUrls($url)
    {
        $libopml = new LibOpml();
        $result = $libopml->checkHttpAddress($url);

        $this->assertFalse($result);
    }

    public function numericHeadElementsProvider()
    {
        return [
            ['vertScrollState'],
            ['windowTop'],
            ['windowLeft'],
            ['windowBottom'],
            ['windowRight'],
        ];
    }

    public function validUrlsProvider()
    {
        return [
            ['http://example.com'],
            ['https://example.com'],
            ['https://éxample.com'],
        ];
    }

    public function invalidUrlsProvider()
    {
        return [
            [''],
            ['http://'],
            ['ftp://example.com'],
            ['ssh://example.com'],
        ];
    }
}
