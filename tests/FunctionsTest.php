<?php

namespace marienfressinaud\LibOpml;

class FunctionsTest extends \PHPUnit\Framework\TestCase
{
    public function testLibopmlParseFileBehavesLikeClass()
    {
        $filepath = __DIR__ . '/../examples/example.opml.xml';

        $opml_array = \libopml_parse_file($filepath);

        $this->assertSame('2.0', $opml_array['version']);

        $this->assertSame('My OPML', $opml_array['head']['title']);
        $this->assertSame(1658758866, $opml_array['head']['dateCreated']->getTimestamp());
    }

    public function testParseStringBehavesLikeClass()
    {
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head>
        <title>My OPML</title>
    </head>
    <body>
        <outline text="Newspapers"/>
    </body>
</opml>
XML;

        $opml_array = \libopml_parse_string($xml);

        $this->assertSame('2.0', $opml_array['version']);

        $this->assertSame('My OPML', $opml_array['head']['title']);

        $outlines = $opml_array['body'];
        $this->assertSame(1, count($outlines));
        $this->assertSame('Newspapers', $outlines[0]['text']);
    }

    public function testRenderBehavesLikeClass()
    {
        $opml = [
            'head' => [
                'title' => 'My OPML',
            ],
            'body' => [
                [
                    'text' => 'Newspapers',
                ],
            ],
        ];
        $expected = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>My OPML</title>
  </head>
  <body>
    <outline text="Newspapers"/>
  </body>
</opml>

XML;

        $string = \libopml_render($opml);

        $this->assertSame($string, $expected);
    }
}
