Merge request checklist:

- [ ] code is manually tested
- [ ] changes concern the parsing and the rendering
- [ ] tests are updated
- [ ] documentation is updated (including comments, commit messages, migration notes, …)

_If you think one of the item isn’t applicable to the MR, please check it
anyway and precise `N/A` next to it._
