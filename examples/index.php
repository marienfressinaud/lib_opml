<?php

include('../vendor/autoload.php');
$filename = 'example.opml.xml';

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>lib_opml examples</title>
    </head>
    <body>

<h1>Test libopml_parse_file</h1>

<?php
try {
    $opml_array1 = libopml_parse_file($filename);
    // same as
    // $libopml = new \marienfressinaud\LibOpml\LibOpml(false);
    // $opml_array1 = $libopml->parseFile($filename);
    echo '<pre>';
    print_r($opml_array1);
    echo '</pre>';
} catch (LibOpml\Exception $e) {
    echo $e->getMessage();
}
?>


<h1>Test libopml_parse_string</h1>

<?php
$opml_string = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<opml version="2.0">
    <head></head>
    <body>
        <outline text="A" />
        <outline text="Simple" />
        <outline text="OPML" />
        <outline text="String" />
    </body>
</opml>
XML;

try {
    $opml_array2 = libopml_parse_string($opml_string);
    // same as
    // $libopml = new \marienfressinaud\LibOpml\LibOpml(false);
    // $opml_array2 = $libopml->parseString($opml_string);
    echo '<pre>';
    print_r($opml_array2);
    echo '</pre>';
} catch (LibOpml\Exception $e) {
    echo $e->getMessage();
}
?>


<h1>Test libopml_render</h1>

<?php
try {
    $opml_string = libopml_render($opml_array1);
    // same as
    // $libopml = new \marienfressinaud\LibOpml\LibOpml(true);
    // $opml_string = $libopml->render($opml_array1);
    echo '<pre>';
    echo htmlspecialchars($opml_string, ENT_NOQUOTES, 'UTF-8');
    echo '</pre>';
} catch (LibOpml\Exception $e) {
    echo $e->getMessage();
}
?>

    </body>
</html>
