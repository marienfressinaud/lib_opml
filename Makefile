.DEFAULT_GOAL := help

ifndef COMPOSER_BIN
	COMPOSER_BIN = composer
endif

ifndef COVERAGE
	COVERAGE = --coverage-html ./coverage
endif

ifdef FILTER
	PHPUNIT_FILTER = --filter=$(FILTER)
else
	PHPUNIT_FILTER =
endif

ifdef FILE
	PHPUNIT_FILE = $(FILE)
else
	PHPUNIT_FILE = ./tests
endif

.PHONY: install
install: ## Install the dependencies
	$(COMPOSER_BIN) install

.PHONY: test
test: ## Run the test suite
	XDEBUG_MODE=coverage \
	php ./vendor/bin/phpunit \
		--bootstrap ./vendor/autoload.php \
		--testdox \
		--whitelist ./src \
		$(COVERAGE) \
		$(PHPUNIT_FILTER) \
		$(PHPUNIT_FILE)

.PHONY: lint
lint: ## Run the linter on the PHP files
	php ./vendor/bin/phpcs --extensions=php --standard=PSR12 ./src ./tests ./examples

.PHONY: lint-fix
lint-fix: ## Fix the errors detected by the linter
	php ./vendor/bin/phpcbf --extensions=php --standard=PSR12 ./src ./tests ./examples

.PHONY: help
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
